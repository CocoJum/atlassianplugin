package ut.com.mycompany.bitbucket.components;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.mycompany.bitbucket.components.MYComponentImpl;

import static org.mockito.Mockito.*;

/**
 * @since 3.5
 */
public class MYComponentImplTest {

    @Before
    public void setup() {

    }

    @After
    public void tearDown() {

    }

    @Test(expected=Exception.class)
    public void testSomething() throws Exception {

        //MYComponentImpl testClass = new MYComponentImpl();

        throw new Exception("MYComponentImpl has no tests!");

    }

}
